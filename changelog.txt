1.4.0
-reward rolls are done per person. not everyone will get the same reward
-messages will only be sent to the players in the configured world
-zombie kills will only count if they are in the configured world
-manually starting an apocalypse will only change the time to 9PM only if there are people available to play
-default zombies per person can be configured
-works with 1.5.1

1.3.1
-edited the message if zombies don't spawn from "you have avoided the undead"
-added /za kills to check current kills
-updated /za commands to show updated and missing commands
-added some variety to chatcolors for ZA messages
-successful commands no longer echo /za back
-prevents you from sleeping during the apocalypse
-added manual start with specified number of zombies


1.2.4 Changes
-made it so no zombies spawn if nobody is on the configured world, previously it would only check if nobody was online
-made it so you couldn't sleep past the morning check
-only OPs can do /za start
-warning at 8:00 PM
-edited /za help